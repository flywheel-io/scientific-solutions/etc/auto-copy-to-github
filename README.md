# Gitlab to Github Auto Mirroring
## Intro
The purpose of this script is to perform an action like a `git mirror`, but to automatically update based on some schedule, so new changes to the Gitlab branch will be brought in regularly.  

By keeping this workflow in github, you only need to manage your own credentials in the github repo that you personally control.  The github repo itself can be private if you so choose.  However the gitlab repo must be public for this workflow to work.

## Definitions:
- **SOURCE REPO**: The gitlab repository that will be the SOURCE of the code.   Code will be copied FROM the SOURCE repo.
- **CLONE REPO**: The github repository that will be a CLONE of the SOURCE repo.  Code will be copied INTO the CLONE repo.

## Instructions
  
  ### Create a new repository and migrate the template workflow
1. Create a repository you control to act as the CLONE REPO.
1. from the top navigation tabs, click "Actions", and select "New Workflow"  
1. Select "set up a workflow yourself", and copy and paste the entire "auto-mirror.yaml" file into the git file editor.  I recommend you rename the file something useful, as the file name determines the name of the workflow.  

![new workflow](/src/setup_workflow.png){width=50%}

### Modify the template 
IN THE NEWLY CREATED FILE IN THE EDITOR:  
1. Fill out the "CLONE_REPO_GROUP" and "CLONE_REPO_NAME" with the repo you just created.  
	  - e.x. If you created the repo: "https://github.com/my_group/my_repo",  
  then you would populate the following: 
		  ```bash
		  CLONE_REPO_GROUP : "my_group" 
		  CLONE_REPO_NAME : "my_repo"  
		```
2. Paste the https address of the SOURCE repository you wish to clone into "SOURCE_REPO_URL".  For example:
	```bash
	  SOURCE_REPO_URL: 'https://gitlab.com/clone-group/clone-project/clone-repo'
	```

### Setup SSH Secrets
1. In a terminal window, generate a new ssh key using "ssh-keygen".  Name it something useful like  "Github_Workflow_Bot".  Use [these steps](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#generating-a-new-ssh-key) to create a proper ssh key for github.  Your command should look something like: 
`ssh-keygen -t ed25519 -f <path_to_key>/<name_of_key> -C "your_email@example.com" `
1. In the newly created clone repository, go to "settings" -> "Deploy Keys" under "security"   
 in the left-side navigation bar.  

![new workflow](/src/add_deploy_key.png){width=60%}

1. Click "Add deploy key", add a label, paste the PUBLIC ssh key you just created in this box.   
   MAKE SURE YOU SELECT "Allow write access".  Now save the key  

![new workflow](/src/allow_write_access.png){width=40%}


1. Now click "Secrets and Variables" -> "actions" in the left-side navigation bar.

![new workflow](src/create_deploy_key.png){width=50%}

1. Click "New Repository Secret", name it "PRIVATE_WORKER_KEY" , and paste the PRIVATE ssh key you just   
   created here and save


This should now work.  Honestly if it doesn't just go to chat gpt and ask what the problem is. 